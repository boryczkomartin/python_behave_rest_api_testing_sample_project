@api @postAnAppFeature
Feature: Post an app feature

#=====================================================================
  
  @postAnAppScenario
  Scenario Outline: Post an app <expected_result>
    Given I have the payload <payload>
    When I send POST /apps/ request
    Then the response status code should be <status_code>
    And the response json schema for single object should be validated <should_validate>

    Examples:
      | payload                                                                                                           | status_code | should_validate | expected_result | 
      | {"name": "STRING_TO_REPLACE", "type": "web","urls": ["https://example0.com"]}                                     | 200         | -true           | -positive |
      | {"name": "STRING_TO_REPLACE", "type": "mobile","urls": ["https://example0.com"]}                                  | 200         | -true           | -positive |
      | {"name": "STRING_TO_REPLACE", "type": "sharepoint","urls": ["https://example0.com"]}                              | 200         | -true           | -positive |
      | {"name": "STRING_TO_REPLACE", "type": "incorrect","urls": ["https://example0.com"]}                               | 422         | -false          | -negative incorrect type |
      | {"name": "STRING_TO_REPLACE", "type": "mobile","urls": ["http://example0.com"]}                                   | 200         | -true           | -positive |
      | {"name": "STRING_TO_REPLACE", "type": "mobile","urls": ["http://example0.com", "https://example0.com"]}           | 200         | -true           | -positive |
      | {"name": "STRING_TO_REPLACE", "type": "mobile","urls": ["http:/www.example0.com"]}                                | 422         | -false          | -negative incorrect url format |
      | {"name": "", "type": "mobile","urls": ["http://www.example0.com"]}                                                | 422         | -false          | -negative name string min length |
      | {"name": "#$%^&^.,*(`#!!)", "type": "mobile","urls": ["http://www.example0.com"]}                                 | 422         | -false          | -negative name string special characters validation |
      | {"name": "long string longlong string longlong string longlong stringkk", "type": "web","urls": ["www.exam.com"]} | 422         | -false          | -negative name string max length |
