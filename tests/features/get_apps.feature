@api @getAppsFeature
Feature: Get apps feature

#=====================================================================

  @getAppsScenario
  Scenario Outline: Get apps <expected_result>
    Given I have query params <offset> <limit> <search>
    When I send GET apps request
    Then the response status code should be <status_code>
    and the response json schema for objects list should be validated <should_validate>

    Examples:
      | offset | limit | search       | expected_result                    | status_code | should_validate |
      | 1      | 5     | empty        | -positive                          | 200         | -true           |
      | 0      | 5     | empty        | -negative offset greater than 0    | 422         | -false          |
      | 100    | 5     | empty        | -negative offset less than 100     | 422         | -false          |
      | 1      | 0     | empty        | -negative limit greater than 0     | 422         | -false          |
      | 1      | 100   | empty        | -negative limit less than 100      | 422         | -false          |
      | 1      | 5     | *^&$%#$%>,./ | -negative special chars validation | 422         | -false          |
      | 1      | 5     | to_long_string_to_long_string_to_long_string_to_long_string_to_long_string_to_long_string_to_long_string_to_long_string_to_long_string_to_long_string_to_long_string_to_long_string  | -negative search string length is limited | 422 | -false |
