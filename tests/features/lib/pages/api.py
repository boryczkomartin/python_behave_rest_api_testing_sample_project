import random
import requests

class Api():
    def __init__(self, context):
        self.__context = context
        self.__context.browser
        self.__headers = {'Content-Type': 'application/json'}

        # TODO:
        # IP address should be set as Env variable, not here. /apps/ should be set as endpoint ENUM TYPE.
        self.__url = 'http://10.243.5.2/apps/'

    def set_query_params(self, offset, limit, search):
        self.__context.params = {}

        if offset != "empty":
            self.__context.params.update( {'offset' : offset} )

        if limit != "empty":
            self.__context.params.update( {'limit' : limit} )

        if search != "empty":
            self.__context.params.update( {'search' : search} )

    def set_payload(self, payload):
        self.__context.payload = payload

    def get_apps_request(self):
        self.__context.response = requests.get(url=self.__url, headers=self.__headers, params=self.__context.params)

    def get_app_request(self):
        self.__context.response = requests.get(url=self.__url + self.__context.app_id, headers=self.__headers)

    def post_app_request(self):
        self.__context.response = requests.post(url=self.__url, headers=self.__headers, data=self.__context.payload)

    def delete_app_request(self):
        self.__context.response = requests.delete(url=self.__url + self.__context.app_id, headers=self.__headers)

    def patch_app_request(self):
        self.__context.response = requests.patch(url=self.__url + self.__context.app_id, headers=self.__headers, data=self.__context.payload)

    def get_status_code(self, status_code):
        assert self.__context.response.status_code == int(status_code)

    def get_random_app_id(self):
        json_data = requests.get(url=self.__url, headers=self.__headers).json()

        # TODO: UNKNOW ISSUE FOR @patchAnAppScenario - SERVER ERROR 500
        # self.__context.app_id = json_data[random.randrange(0, len(json_data)-1)]['id']
        #
        # TEMPORARY WORKAROUND:
        self.__context.app_id = json_data[0]['id']

    def set_fake_app_id(self, app_id):
        self.__context.app_id = app_id

    def get_app_data(self):
        self.__context.app_data = requests.get(url=self.__url + self.__context.app_id, headers=self.__headers)
        self.__context.name = self.__context.app_data.json()['name']
        self.__context.type = self.__context.app_data.json()['type']
        self.__context.urls = self.__context.app_data.json()['urls']

    def change_app_data(self, name, type, urls):
        if name != "empty":
            self.__context.name = name

        if type != "empty":
            self.__context.type = type

        if urls != "empty":
            self.__context.urls = urls

        self.__context.payload = '{ "name": "' + self.__context.name + \
            '", "type": "'+ self.__context.type + \
            '", "urls": '+ self.__context.urls +' }'

    def __validation_json_schema(self, json_data):
        assert 'name' in json_data
        assert 'type' in json_data
        assert 'urls' in json_data
        assert 'id' in json_data
        assert 'created_at' in json_data        

    def validate_json_response_app(self):
        json_data = self.__context.response.json()
        self.__validation_json_schema(json_data)


    def validate_json_response_apps(self):
        json_data = self.__context.response.json()[0]
        self.__validation_json_schema(json_data)

    def validate_json_response_error(self):
        json_data = self.__context.response.json()
        assert 'detail' in json_data
        assert 'loc' in json_data['detail'][0]
        assert 'msg' in json_data['detail'][0]
        assert 'type' in json_data['detail'][0]