import string
import random

class Utils():

    def generate_random_string(self, stringLength = 10):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(stringLength))