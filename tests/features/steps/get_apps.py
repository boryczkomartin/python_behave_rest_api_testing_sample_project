from behave import given, when, then
from features.lib.pages import *

@given(u'I have query params {offset} {limit} {search}')
def set_query_params(context, offset, limit, search):
    Api(context).set_query_params(offset, limit, search)

@when(u'I send GET apps request')
def get_apps_request(context):
    Api(context).get_apps_request()

@then(u'the response json schema for objects list should be validated {should_validate}')
def validate_json_response_apps(context, should_validate):
    if should_validate == '-true':
        Api(context).validate_json_response_apps()
    else:
        Api(context).validate_json_response_error()
