from behave import when
from features.lib.pages import *

@when(u'I send DELETE /apps/app_id request')
def delete_app_request(context):
    Api(context).delete_app_request()