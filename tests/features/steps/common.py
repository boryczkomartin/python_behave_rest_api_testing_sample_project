from behave import given, then
from features.lib.pages import *

@given(u'I have the payload {payload}')
def set_payload(context, payload):
    payload = payload.replace('STRING_TO_REPLACE', Utils().generate_random_string())
    Api(context).set_payload(payload)

@given(u'I have app id {app_id}')
def get_random_app_id(context, app_id = "random"):
    if app_id == 'random':
        Api(context).get_random_app_id()
    else:
        Api(context).set_fake_app_id(app_id)

@then(u'the response status code should be {status_code}')
def get_recived_status_code(context, status_code):
    Api(context).get_status_code(status_code)

@then(u'the response json schema for single object should be validated {should_validate}')
def validate_json_response_app(context, should_validate):
    if should_validate == '-true':
        Api(context).validate_json_response_app()
    elif should_validate == '-false' and context.response.status_code != 404:
        Api(context).validate_json_response_error()