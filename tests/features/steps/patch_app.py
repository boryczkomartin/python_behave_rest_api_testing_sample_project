from behave import given, when
from features.lib.pages import *

@given(u'I have app data')
def get_app_data(context):
    Api(context).get_app_data()

@when(u'I send PATCH /apps/app_id request')
def patch_app_request(context):
    Api(context).patch_app_request()

@when(u'I change app data {name} {type} {urls}')
def change_app_data(context, name, type, urls):
    Api(context).change_app_data(name, type, urls)