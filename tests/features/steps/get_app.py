from behave import when
from features.lib.pages import *

@when(u'I send GET /apps/app_id request')
def get_app_request(context):
    Api(context).get_app_request()