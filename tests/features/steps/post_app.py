from behave import when, then
from features.lib.pages import *

@when(u'I send POST /apps/ request')
def post_app_request(context):
    Api(context).post_app_request()