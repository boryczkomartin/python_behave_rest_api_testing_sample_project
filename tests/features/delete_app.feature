@api @deleteAnAppFeature
Feature: Delete an app feature

#=====================================================================

  @deleteAnAppScenario
  Scenario Outline: Delete an app <expected_result>
    Given I have app id <app_id>
    When I send DELETE /apps/app_id request
    Then the response status code should be <status_code>

    Examples:
      | app_id                               | expected_result             | status_code |
      | e74847d1-adb7-4213-903b-6811dcb94b42 | - negative fake id          | 400         |
      | random                               | - positive                  | 204         |