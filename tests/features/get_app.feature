@api @getAnAppFeature
Feature: Get an app feature

#=====================================================================

  @getAnAppScenario
  Scenario Outline: Get an app <expected_result>
    Given I have app id <app_id>
    When I send GET /apps/app_id request
    Then the response status code should be <status_code>
    And the response json schema for single object should be validated <should_validate>

    Examples:
      | app_id                               | expected_result    | status_code | should_validate |
      | e74847d1-adb7-4213-903b-6811dcb94b42 | - negative fake id | 404         | -false 		  |
      | random                               | - positive         | 200         | -true 	      |