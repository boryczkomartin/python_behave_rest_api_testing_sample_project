@api @patchAnAppFeature
Feature: Patch an app feature

#=====================================================================

  @patchAnAppScenario
  Scenario Outline: Patch an app <expected_result>
    Given I have app id <app_id>
    And I have app data
    When I change app data <name> <type> <urls>
    And I send PATCH /apps/app_id request
    Then the response status code should be <code>
    And the response json schema for single object should be validated <should_validate>

    Examples:
      | app_id | name   | type       | urls                   | code | should_validate | expected_result |
      | random | random | mobile     | ["http://example.com"] | 200  | -true           | -positive       |
      | random | random | web        | ["http://example.com"] | 200  | -true           | -positive       |
      | random | random | sharepoint | ["http://example.com"] | 200  | -true           | -positive       |
      | random | random | incorrect  | ["http://example.com"] | 422  | -false          | -negative incorrect type |
      | random | random | web        | ["http:/example.com"]  | 422  | -false          | -negative incorrect url  |
      | random | %^&*#@ | web        | ["http://example.com"] | 422  | -false          | -negative name special characters validation |
      | random |   i    | web        | ["http://example.com"] | 422  | -false          | -negative name min length |
      | random | fjiffnfekmrrfjiffnfekmrrfjiffnfekmrrfjiffnfekmrrfjiffnfekmrrf | web | ["http://example.com"] | 422  | -false | -negative name max length |